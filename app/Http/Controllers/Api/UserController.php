<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function register(Request $request)
    {
        try {
            $databody = $request->all();
            $validator = Validator::make($databody,[
                "name"=> "required",
                "email"=> "required|email|unique:users,email",
                "password_"=> "required|confirmed",
                "password_confirmation" => "required",
            ]);
            if ($validator->fails()) {
                return response()->json([
                    "status"=>false,
                    "message"=>"Erreur de validation",
                    "errors"=> $validator->errors()
                ],422);
            }

            $databody['password']=Hash::make($request->password);

            // create user according User schema
            $user = User::create($databody);

            return response()->json([
                "status"=>true,
                "message"=>"User created !",
                "data"=> [
                    "token" => $user->createToken('auth_user')->plainTextToken,
                    "token_type" => "Bearer",
                ],
            ]);

        } catch (\Throwable $th) {
            return response()->json([
                "status"=>false,
                "message"=>$th->getMessage()
            ],500);
        }
    }

    public function login(Request $request)
    {   
        // try {
        //     //code...
        // } catch (\Throwable $th) {
        //     //throw $th;
        // }
        return "LOGIN";
    }
}
